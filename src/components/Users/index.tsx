import React from 'react';
import { ButtonRemove, Container, UserName} from "./styles";
import {Text, TouchableOpacityProps} from "react-native";

interface IUserProps extends TouchableOpacityProps{
    name: string;
    onRemove: () => void;
    onEdit: () => void;
}

export const Users = ({name, onRemove, onEdit}: IUserProps) => {
    return(
        <Container onPress={onEdit}>
            <UserName >
                {name}
            </UserName>
            <ButtonRemove onPress={onRemove}>
                <Text>-</Text>
            </ButtonRemove>
        </Container>
    )
}
