import styled from "styled-components/native";

export const Container = styled.TouchableOpacity`
  width: 100%;
  background: #1F1E25;
  
  margin-bottom: 5px;
  
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-direction: row;
  
  border-radius: 5px;
`;

export const ButtonRemove = styled.TouchableOpacity`
  width: 15%;
  border-radius: 5px;
  background: red;
  padding: 15px;
  
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const UserName = styled.Text`
  color: white;
  font-size: 16px;
  font-weight: bold;
  
  margin: 15px 0 15px 15px;
`;
