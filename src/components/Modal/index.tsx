import {ButtonCancell, ButtonContent, ButtonSave, Container, Content, InputTextUpdate, ModalUp} from "./styles";
import {useState} from "react";
import {Alert, ModalProps, Pressable, Text} from "react-native";

interface IModalProps extends ModalProps {
    userToEdit: string;
    visible: boolean;
    onRequestClose: () => void;
    handleUserUpdate: (oldName: string, newName: string) => void;
}

export const ModalUpdate = ({userToEdit, visible, onRequestClose, handleUserUpdate}: IModalProps) => {
    const [newUserName, setNewUserName ] = useState('');

    const handleUpdate = () => {
        if (newUserName.trim() === '') {
            Alert.alert('Nome invalido');
            return;
        }

        handleUserUpdate(userToEdit, newUserName);
        onRequestClose();
    }

    return (
        <ModalUp
            visible={visible}
            onRequestClose={onRequestClose}
        >
            <Container>
                <Content>
                    <Text style={{color: 'white', fontWeight: 'bold', fontSize: 18, textAlign: 'center'}}>Digite o novo nome para o usuario {newUserName}</Text>
                    <InputTextUpdate
                        style={{color: 'white'}}
                        value={newUserName}
                        onChangeText={(text) => {
                            setNewUserName(text);
                        }
                    }/>
                    <ButtonContent>
                        <ButtonCancell
                            onPress={onRequestClose}>
                            <Text
                                style={{color: 'white'}}
                            >Cancell</Text>
                        </ButtonCancell>
                        <ButtonSave onPress={handleUpdate}>
                            <Text style={{color: 'white', fontWeight: 'bold'}}>Salvar</Text>
                        </ButtonSave>
                    </ButtonContent>

                </Content>
            </Container>
        </ModalUp>
    )
}
