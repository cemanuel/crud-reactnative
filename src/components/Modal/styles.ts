import styled from "styled-components/native";
import {Modal} from 'react-native';
import {Input} from "../input";

export const Container = styled.View`
  
  flex: 1;
  display: flex;
  align-items: center;
  justify-content: center;
`;


export const Content = styled.View`
  height: 200px;
  width: 70%;
  background: #1F1E25;
  border-radius: 5px;
  padding: 30px;
  
  align-items: center;
  justify-content: space-between;
`;

export const ModalUp = styled(Modal).attrs({
    animationType: "slide",
    transparent: true,
    shadowColor: 'f1f1f1',
    shadowOffset: {
        width: 0,
        height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
})`
  background: white;
`;

export const ButtonCancell = styled.TouchableOpacity`
  width: 80px;
  background: transparent;
  padding: 5px;
  
  border: 1px solid white;
  border-radius: 5px;
  align-items: center;
`;

export const ButtonSave = styled.TouchableOpacity`
  width: 80px;
  background: #31CF67;
  padding: 5px;

  border: 1px solid #31CF67;
  border-radius: 5px;
  
  align-items: center;
  margin-left: 10px;
  
`;

export const ButtonContent = styled.View`
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-direction: row;
`;

export const InputTextUpdate = styled.TextInput`
  width: 100%;
  background: transparent;
  border: 1px solid #131016;
  font-size: 15px;
  padding: 5px;
  border-radius: 5px;
`
