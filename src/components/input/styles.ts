import styled from 'styled-components/native';

export const Container = styled.TextInput.attrs({
    placeholderTextColor: '#FDFCFE',
})`
  display: flex;
  width: 80%;
  padding: 16px;
  font-size: 15px;
  color: #FDFCFE;
  border-radius: 5px;
  background: #1F1E25;
`;
