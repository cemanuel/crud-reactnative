import React from 'react';
import {Container} from "./styles";
import {TextInputProps} from "react-native";

interface IInputProps extends TextInputProps{}

export const Input = ({...rest}:IInputProps) => {
    return(
        <Container {...rest}/>
    )
}
