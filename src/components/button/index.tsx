import {Container} from "./styles";
import {TouchableOpacityProps} from "react-native";

interface IButtonProps extends TouchableOpacityProps{}

export const Button = ({...rest}:IButtonProps) => {
    return (
        <Container {...rest}>
        </Container>
    )
}
