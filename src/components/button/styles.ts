import styled from "styled-components/native";

export const Container = styled.TouchableOpacity`
  width: 15%;
  padding: 16px;
  border-radius: 5px;
  background: #31CF67;
  
  display: flex;
  align-items: center;
  justify-content: center;
  
  margin-left: 15px;
`
