// Importações necessárias do React e do React Native
import React, {useState} from 'react';
import {Alert, FlatList, Modal, StyleSheet, Text, View} from 'react-native';

// Importações dos componentes customizados
import {Input} from "./src/components/input";
import {Button} from "./src/components/button";
import {Users} from "./src/components/Users";
import {ModalUpdate} from "./src/components/Modal";

export default function App() {
    // Declaração dos estados iniciais para os usuários e o nome do novo usuário
    const [ users, setUsers ] = useState<string[]>([]);
    const [ userName, setUserName ] = useState('');
    const [ modalVisible, setModalVisible ] = useState(false);
    const [ userToEdit, setUserToEdit ] = useState('');


    // Função chamada ao adicionar um novo usuário à lista
    function handleUsersAdd() {
        // Verifica se já existe um usuário com esse nome na lista
        if(users.includes(userName)){
            // Se já existir, exibe um alerta na tela
            return Alert.alert("Já existe um usuario com este nome na lista!");
        }

        // Adiciona o novo usuário à lista de usuários
        setUsers([...users, userName]);
        // Limpa o campo de nome do novo usuário
        setUserName('');
    }

    // Função chamada ao remover um usuário da lista
    function handleRemoveUsers(name: string) {
        // Exibe um alerta para confirmar a remoção do usuário
        Alert.alert("Remover", `Deseja remover o usuario ${name}?`, [
            {
                text: 'Sim',
                onPress: () => setUsers(prevState => prevState.filter(user => user !== name))
            },
            {
                text: 'Não',
                style: 'cancel'
            }
        ])
    }

    function handleUserUpdate(oldName: string, newName: string) {
        if(users.includes(newName)) {
            return Alert.alert('Já existe um usuario com este nome na lista!');
        }

        setUsers(prevState => prevState.map(user => user === oldName ? newName : user));
        setUserToEdit('');
        setModalVisible(false);
    }

    // Componente principal que será renderizado
    return (
        <View style={styles.container}>
            <Text
                style={{
                    color: 'white',
                    fontSize: 20,
                    fontWeight: 'bold',
                    marginTop: 60,
                }}
            >My App CRUD</Text>

            {/* Componente de entrada de texto para o nome do novo usuário */}
            <View style={styles.header}>
                <Input
                    placeholder='Digite um nome'
                    placeholderTextColor='#fff'
                    value={userName}
                    onChangeText={setUserName}
                />

                {/* Botão para adicionar um novo usuário à lista */}
                <Button onPress={handleUsersAdd}>
                    <Text>+</Text>
                </Button>
            </View>

            {/* Título da lista de usuários */}
            <Text style={styles.title}>
                Participantes
            </Text>

            {/* Lista de usuários */}
            <FlatList
                data={users}
                keyExtractor={(item) => item}
                renderItem={({item}) => (

                    // Componente customizado que exibe o nome do usuário e permite removê-lo
                    <Users
                        key={item}
                        name={item}
                        onRemove={() => handleRemoveUsers(item)}
                        onEdit={() => {
                            setModalVisible(true)
                            setUserToEdit(item)
                        }}
                    />
                )}
                showsVerticalScrollIndicator={false}
            />
            <ModalUpdate
                handleUserUpdate={handleUserUpdate}
                userToEdit={userToEdit}
                visible={modalVisible}
                onRequestClose={() => {
                    setModalVisible(false);
                }}
            />
        </View>
    );
}

// Estilos para os componentes
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#131016',
        display: 'flex',
        justifyContent: 'center',
        padding: 20,
    },
    header: {
        width: '100%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        marginTop: 80,
    },
    title: {
        fontSize: 20,
        color: '#FDFCFE',
        textAlign: "left",
        marginTop: 42,
        padding: 5,
    },
    modalView: {
        margin: 20,
        backgroundColor: 'white',
        borderRadius: 20,
        padding: 35,
        alignItems: 'center',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
    },
});
