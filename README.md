
<h1>CRUD React Native App</h1>
<p>Este repositório contém o código fonte de um aplicativo CRUD (Create, Read, Update, Delete) desenvolvido usando React Native. O aplicativo permite que o usuário crie, leia, atualize e exclua informações em tempo real.</p>
<h2>Instalação</h2>
<p>Para instalar o aplicativo, siga as instruções abaixo:</p>
<ol>
    <li>Clone o repositório:</li>
    <pre><code>git clone https://gitlab.com/cemanuel/crud-reactnative.git</code></pre>
    <li>Navegue para o diretório do projeto:</li>
    <pre><code>cd crud-reactnative</code></pre>
    <li>Instale as dependências:</li>
    <pre><code>npm install</code></pre>
    <li>Inicie o aplicativo:</li>
    <pre><code>npx react-native run-android</code></pre>
    <pre><code>npx react-native run-ios</code></pre>
    <pre><code>npx expo start</code></pre>
    <pre><code>expo run:ios</code></pre>
    <pre><code>expo run:android</code></pre>
</ol>
<h2>Tecnologias utilizadas</h2>
<ul>
    <li>React Native</li>
    <li>Expo</li>
    <li>typescript</li>
    <li>styled-components</li>
</ul>
<h2>Contribuição</h2>
<p>Contribuições são bem-vindas! Para fazer uma contribuição, siga estas etapas:</p>
<ol>
    <li>Faça um fork do repositório</li>
    <li>Crie uma nova branch:</li>
    <pre><code>git checkout -b minha-nova-feature</code></pre>
    <li>Commit as alterações:</li>
    <pre><code>git commit -am 'Adicionei uma nova feature'</code></pre>
    <li>Push para o branch:</li>
    <pre><code>git push origin minha-nova-feature</code></pre>
    <li>Crie um pull request para a branch principal</li>
</ol>
<h2>Autor</h2>
<p>Este aplicativo foi desenvolvido por Cleydson Emanuel.</p>

